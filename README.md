# procfetch

#### procfetch is a command-line tool to fetch system information and display it on the screen. 
#### It is written in `C++`. 
#### Project inspiration : `neofetch` - a command line system information tool

### Information displayed by procfetch
#### Static characteristics

* Host (User)
* CPU
* Kernel
* OS
* Shell
* Desktop Enviroment
* Resolution
* Theme
* Icons
* GPU (Integrated & Dedicated)
* Package count

#### Dynamic characteristics
* RAM
* Uptime
* Temperature

## Installation

```
git clone https://gitlab.com/aurum4/procfetch.git
 ```

```
cd procfetch
```

 ```
sudo apt install ./procfetch_1.0_all.deb
```
```
sudo chmod +x /usr/bin/procfetch
```


![](./images/tty.gif)


#### Disclamer :
Tried and tested on Debian, Ubuntu and ParrotOS.
